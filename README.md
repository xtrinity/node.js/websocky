# Websocky - README

Check `./example/` for usage examples.

## Usage

RPC calls can be registered on-the-fly but for most use cases they should be registered before server initialization.

## Notes

* Synchronous batching vs asynchronous batching
  - How does asynchronous remember execution? (call id with reverse server to client push)
  - What if we have too many unhandled calls? Is WebSocks handling that?
* Understand WebSocket bandwidth reduction (HTTP Headers)
* Understand how load-balancing would work (Sticky Sessions)
  - Can this be baked into WebSocks through setting request and response headers?
  - Nginx proxies requests by creating a tunnel between Server and Client
* Understand how the Method/Action layer works for routing and how it reduces namespace
* Understand how versioning would hypothetically work. Use of semver creates backwards compatible semantics
* There are a few layers of namespace access. API user scope, method scope, and action scope
* Use of language for methods should include get, make, create, query, and etc. 
* Create version handling for better scoping