"use strict";

// Dependency
const WebSocket = require("ws");
const Promise = require("bluebird");
const Color = require("colors");

// Class
function WebSocksServer(config) {
  this.server = null;
  this.ws = {
    "hostname": config.hostname || "localhost",
    "port": config.port || 8080
  };
  this.auth = {
    "db": [],
    "authFunction": config.authFunction,
    "deauthFunction": config.deauthFunction
  };
  this.publicRegistry = {};
  this.privateRegistry = {};
  this.log = config.log || false;
  this.noLogMethod = [];
  this.errorHandler = config.errorHandler;
}

WebSocksServer.prototype.noLog = function (method) {
  this.noLogMethod.push(method);
};

WebSocksServer.prototype.registerRPC = function (methodName, handler, authRole) {
  if (!methodName || typeof methodName !== "string") throw new Error("methodName must be a string.");
  else if (!handler || typeof handler !== "function") throw new Error("handler must be a function.");
  if (!authRole || typeof authRole !== "string") this.publicRegistry[methodName] = handler;
  else {
    if (typeof this.privateRegistry[authRole] !== "object") this.privateRegistry[authRole] = {};
    this.privateRegistry[authRole][methodName] = handler;
  }
};

WebSocksServer.prototype.initialize = async function () {
  const self = this;
  return new Promise(function (resolve, reject) {
    self.server = new WebSocket.Server({
      "host": self.ws.hostname,
      "port": self.ws.port
    });
    self.server.on("connection", function (websocket, req) {
      websocket.on("message", async function (payload) {
        try {
          let jsonPayload = JSON.parse(payload.toString());
          let startDelta = 0;
          let response = null;
          if (self.log === true) {
            startDelta = new Date().getTime();
            log.call(self, jsonPayload, 0, payload.length);
          }
          if (jsonPayload.type === "authenticate") {
            response = await authenticate.call(self, websocket, jsonPayload, req);
          } else if (jsonPayload.type === "deauthenticate") {
            response = await deauthenticate.call(self, websocket, jsonPayload, req);
          } else if (jsonPayload.type === "list-rpc") {
            response = await listRPC.call(self, websocket, jsonPayload, req);
          } else if (jsonPayload.type === "call-rpc") {
            response = await callRPC.call(self, websocket, jsonPayload, req);
          }
          if (self.log === true && response) log.call(self, response, startDelta);
        } catch (error) {
          if (!(error instanceof SyntaxError)) throw error;
        }
      });
    });
    self.server.on("listening", function () {
      return resolve();
    });
  });
};

function log(jsonPayload, startDelta, size) {
  if (jsonPayload.code && typeof jsonPayload.code === "string") {
    const STATUS = Color.bold(Color.magenta("[SENT]"));
    const TYPE = jsonPayload.type;
    const CALL_ID = jsonPayload.callId;
    const SECOND = (new Date().getTime() - startDelta) + "ms";
    if (jsonPayload.type === "call-rpc") {
      const METHOD = Color.bold(Color.green(jsonPayload.method));
      if (this.noLogMethod.indexOf(jsonPayload.method) === -1) {
        console.log(`${STATUS} ${TYPE} ${METHOD}:${CALL_ID} ${SECOND}`);
      }
    } else {
      console.log(`${STATUS} ${TYPE}:${CALL_ID} ${SECOND}`);
    }
  } else {
    const STATUS = Color.bold(Color.magenta("[RECV]"));
    const TYPE = jsonPayload.type;
    const CALL_ID = jsonPayload.callId;
    let AUTH = "-";
    if (jsonPayload.auth && typeof jsonPayload.auth === "object") {
      AUTH = Color.bold(Color.brightBlue(jsonPayload.auth.role + "/" + jsonPayload.auth.username));
    }
    const SIZE = size + " bytes";
    if (jsonPayload.type === "call-rpc") {
      const METHOD = Color.bold(Color.green(jsonPayload.method));
      if (this.noLogMethod.indexOf(jsonPayload.method) === -1) {
        console.log(`${STATUS} ${TYPE} ${METHOD}:${CALL_ID} ${AUTH} ${SIZE}`);
      }
    } else {
      console.log(`${STATUS} ${TYPE}:${CALL_ID} ${AUTH} ${SIZE}`);
    }
  }
}

WebSocksServer.prototype.getServer = function () {
  return this.server;
};

WebSocksServer.prototype.close = async function () {
  const self = this;
  return new Promise(function (resolve, reject) {
    self.server.close(function (error) {
      if (error) return reject(error);
      else return resolve();
    });
  });
};

// Function
async function authenticate(websocket, jsonPayload, req) {
  if (!jsonPayload.auth || typeof jsonPayload.auth !== "object" || typeof jsonPayload.auth.role !== "string"
    || typeof jsonPayload.auth.username !== "string" || (typeof jsonPayload.auth.password !== "string"
      && typeof jsonPayload.auth.token !== "string")) {
    const jsonResponse = {
      "callId": jsonPayload.callId,
      "type": "authenticate",
      "code": "failed"
    };
    await websocket.send(JSON.stringify(jsonResponse));
    return jsonResponse;
  }
  let auth = jsonPayload.auth;
  let authenticated = null;
  if (typeof this.auth.authFunction === "function") {
    auth.ipAddress = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
    authenticated = await this.auth.authFunction(auth);
    if (typeof authenticated === "object") {
      this.auth.db = this.auth.db.filter(function (element, array, index) {
        return !(element.role === auth.role && element.username === auth.username && element.token === auth.token);
      });
      this.auth.db.push({
        "role": authenticated.role,
        "username": authenticated.username,
        "token": authenticated.token,
        "expireDate": authenticated.expireDate
      });
      const jsonResponse = {
        "callId": jsonPayload.callId,
        "type": "authenticate",
        "code": "success",
        "auth": {
          "role": authenticated.role,
          "username": authenticated.username,
          "token": authenticated.token,
          "expireDate": authenticated.expireDate
        }
      };
      await websocket.send(JSON.stringify(jsonResponse));
      return jsonResponse;
    }
  }
  const jsonResponse = {
    "callId": jsonPayload.callId,
    "type": "authenticate",
    "code": "failed"
  };
  await websocket.send(JSON.stringify(jsonResponse));
  return jsonResponse;
}

async function deauthenticate(websocket, jsonPayload) {
  if (!jsonPayload.auth || typeof jsonPayload.auth !== "object" || typeof jsonPayload.auth.role !== "string"
    || typeof jsonPayload.auth.username !== "string" || (typeof jsonPayload.auth.password !== "string"
      && typeof jsonPayload.auth.token !== "string")) {
    const jsonResponse = {
      "callId": jsonPayload.callId,
      "type": "deauthenticate",
      "code": "failed"
    };
    await websocket.send(JSON.stringify(jsonResponse));
    return jsonResponse;
  }
  const auth = jsonPayload.auth;
  let deauthenticated = null;
  if (typeof this.auth.deauthFunction === "function") {
    deauthenticated = await this.auth.deauthFunction(auth);
    if (deauthenticated === true) {
      this.auth.db = this.auth.db.filter(function (element, array, index) {
        return !(element.role === auth.role && element.username === auth.username && element.token === auth.token);
      });
      const jsonResponse = {
        "callId": jsonPayload.callId,
        "type": "deauthenticate",
        "code": "success"
      };
      await websocket.send(JSON.stringify(jsonResponse));
      return jsonResponse;
    }
  }
  const jsonResponse = {
    "callId": jsonPayload.callId,
    "type": "deauthenticate",
    "code": "failed"
  };
  await websocket.send(JSON.stringify(jsonResponse));
  return jsonResponse;
}

function checkAuthentication(auth) {
  if (!auth || typeof auth !== "object") return false;
  const role = auth.role;
  const username = auth.username;
  const token = auth.token;
  let authenticated = false;
  this.auth.db = this.auth.db.filter(function (element, array, index) {
    if (element.expireDate < new Date()) return false;
    if (element.role === role && element.username === username && element.token === token) authenticated = true;
    return true;
  });
  return authenticated;
}

async function listRPC(websocket, jsonPayload) {
  // List Private Registry
  if (jsonPayload.auth && typeof jsonPayload.auth === "object" && jsonPayload.auth.role && this.privateRegistry.hasOwnProperty(jsonPayload.auth.role)) {
    const jsonResponse = {
      "callId": jsonPayload.callId,
      "type": "rpc",
      "code": "failed-authentication"
    };
    await websocket.send(JSON.stringify(jsonResponse));
    return jsonResponse;
  }
  // List Public Registry
  const jsonResponse = {
    "callId": jsonPayload.callId,
    "type": "list-rpc",
    "methods": Object.keys(this.publicRegistry)
  };
  await websocket.send(JSON.stringify(jsonResponse));
  return jsonResponse;
}

async function callRPC(websocket, jsonPayload) {
  if (jsonPayload.auth && typeof jsonPayload.auth === "object" && this.privateRegistry.hasOwnProperty(jsonPayload.auth.role)) {
    if (this.privateRegistry[jsonPayload.auth.role].hasOwnProperty(jsonPayload.method)) {
      const authenticated = checkAuthentication.call(this, jsonPayload.auth);
      if (authenticated === false) {
        const jsonResponse = {
          "callId": jsonPayload.callId,
          "type": "call-rpc",
          "method": jsonPayload.method,
          "code": "failed-authentication"
        };
        await websocket.send(JSON.stringify(jsonResponse));
        return jsonResponse;
      }
      const method = this.privateRegistry[jsonPayload.auth.role][jsonPayload.method];
      let response = {};
      try {
        response = await method(jsonPayload.data);
      } catch (error) {
        const jsonResponse = {
          "callId": jsonPayload.callId,
          "type": "call-rpc",
          "method": jsonPayload.method,
          "code": "server-error",
          "message": "Server encountered an error.",
          "data": {}
        };
        await websocket.send(JSON.stringify(jsonResponse));
        if (typeof this.errorHandler === "function") this.errorHandler(error, jsonPayload);
        else throw error;
      }
      const jsonResponse = {
        "callId": jsonPayload.callId,
        "type": "call-rpc",
        "method": jsonPayload.method,
        "code": response.code || "error-method-handler",
        "message": response.message || "",
        "data": response.data || {}
      };
      await websocket.send(JSON.stringify(jsonResponse));
      return jsonResponse;
    }
    // Private Method does not exist
    const jsonResponse = {
      "callId": jsonPayload.callId,
      "type": "call-rpc",
      "method": jsonPayload.method,
      "code": "invalid-method",
      "message": "Invalid private method."
    };
    await websocket.send(JSON.stringify(jsonResponse));
    return jsonResponse;
  }
  if (!jsonPayload.auth || typeof jsonPayload.auth !== "object") {
    if (this.publicRegistry.hasOwnProperty(jsonPayload.method)) {
      const method = this.publicRegistry[jsonPayload.method];
      let response = {};
      try {
        response = await method(jsonPayload.data);
      } catch (error) {
        const jsonResponse = {
          "callId": jsonPayload.callId,
          "type": "call-rpc",
          "method": jsonPayload.method,
          "code": "server-error",
          "message": "Server encountered an error.",
          "data": {}
        };
        await websocket.send(JSON.stringify(jsonResponse));
        if (typeof this.errorHandler === "function") this.errorHandler(error);
        else throw error;
      }
      const jsonResponse = {
        "callId": jsonPayload.callId,
        "type": "call-rpc",
        "method": jsonPayload.method,
        "code": response.code || "error-method-handler",
        "message": response.message || "",
        "data": response.data || {}
      };
      await websocket.send(JSON.stringify(jsonResponse));
      return jsonResponse;
    }
    // Public Method does not exist
    const jsonResponse = {
      "callId": jsonPayload.callId,
      "type": "call-rpc",
      "method": jsonPayload.method,
      "code": "invalid-method",
      "message": "Invalid public method."
    };
    await websocket.send(JSON.stringify(jsonResponse));
    return jsonResponse;
  }
  // Invalid Auth
  const jsonResponse = {
    "callId": jsonPayload.callId,
    "type": "call-rpc",
    "method": jsonPayload.method,
    "code": "invalid-auth",
    "message": "Bad auth credentials or unregistered private or public RPC call."
  };
  await websocket.send(JSON.stringify(jsonResponse));
  return jsonResponse;
}

// Export
module.exports = WebSocksServer;