"use strict";

// Dependencies
const WebSocket = require("ws");
const Promise = require("bluebird");

// Functions
async function send(websocket, data) {
  try {
    return new Promise(function (resolve, reject) {
      if (websocket.readyState === WebSocket.OPEN) {
        websocket.send(JSON.stringify(data), function (error) {
          if (error) return reject(error);
          else return resolve();
        });
      } else return reject(new Error("WebSocket is not open or is no longer open."));
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
}

// Exports
module.exports = {};
module.exports.send = send;