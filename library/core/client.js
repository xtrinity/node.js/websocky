"use strict";

// Dependency
const WebSocket = require("ws");
const Promise = require("bluebird");
const Token = require("./token");
const EventEmitter = require("events");
const Utility = require("./utility");

// Class
function WebSocksClient(config) {
  this.client = null;
  const hostname = config.hostname || "localhost";
  let address = `ws://${config.hostname || "localhost"}:${config.port || 8080}`;
  if (config.secure === true) address = `wss://${config.hostname || "localhost"}:${config.port || 8080}`;
  this.ws = {
    "hostname": hostname,
    "port": config.port,
    "address": address,
    "protocol": config.protocol || {},
    "option": config.option || {},
    "onClose": config.onClose || function () {
    }
  };
  this.auth = {};
  this.rpc = {
    "timeout": config.rpcTimeout || 1000 * 30
  };
  this.authEmitter = new EventEmitter();
  this.rpcEmitter = new EventEmitter();
  this.pubSubEmitter = new EventEmitter();
}

WebSocksClient.prototype.connect = async function () {
  const self = this;
  return new Promise(function (resolve, reject) {
    self.client = new WebSocket(self.ws.address, self.ws.protocol, self.ws.option);
    self.client.on("message", function (payload) {
      try {
        let jsonPayload = JSON.parse(payload);
        if (jsonPayload.type === "authenticate") {
          self.authEmitter.emit(jsonPayload.callId, jsonPayload);
        } else if (jsonPayload.type === "deauthenticate") {
          self.authEmitter.emit(jsonPayload.callId, jsonPayload);
        } else if (jsonPayload.type === "list-rpc") {
          self.rpcEmitter.emit(jsonPayload.callId, jsonPayload);
        } else if (jsonPayload.type === "call-rpc") {
          self.rpcEmitter.emit(jsonPayload.callId, jsonPayload);
        }
      } catch (error) {
        if (!(error instanceof SyntaxError)) throw error;
      }
    });
    self.client.on("close", function () {
      self.ws.onClose();
    });
    self.client.on("open", function () {
      return resolve();
    });
  });
};

WebSocksClient.prototype.authenticate = async function (auth) {
  const self = this;
  if (!auth || typeof auth !== "object") throw TypeError("auth must be an object");
  if (!auth.role || typeof auth.role !== "string") throw TypeError("auth.role must be a string");
  if (!auth.username || typeof auth.username !== "string") throw TypeError("auth.username must be a string");
  if ((!auth.token || typeof auth.token !== "string") && (!auth.password || typeof auth.password !== "string")) {
    throw TypeError("auth.session or auth.password must be a string");
  }
  return new Promise(async function (resolve, reject) {
    const callId = Token.generate();
    const timeout = setTimeout(async function () {
      self.authEmitter.removeAllListeners(callId);
      return resolve({
        "callId": callId,
        "type": "authenticate",
        "code": "timed-out"
      });
    }, self.rpc.timeout);
    self.authEmitter.prependOnceListener(callId, async function (data) {
      clearTimeout(timeout);
      const response = {
        "callId": callId,
        "type": data.type,
        "code": data.code,
      };
      if (typeof data.auth === "object") response.auth = data.auth;
      return resolve(response);
    });
    try {
      const authRequest = {
        "role": auth.role,
        "username": auth.username
      };
      if (auth.token && typeof auth.token === "string") authRequest.token = auth.token;
      if (auth.password && typeof auth.password === "string") authRequest.password = auth.password;
      await Utility.send(self.client, {
        "callId": callId,
        "type": "authenticate",
        "auth": authRequest
      });
    } catch (error) {
      clearTimeout(timeout);
      self.rpcEmitter.removeAllListeners(callId);
      return reject(error);
    }
  });
};

WebSocksClient.prototype.deauthenticate = async function (auth) {
  const self = this;
  if (!auth || typeof auth !== "object") throw TypeError("auth must be an object");
  else if (!auth.role || typeof auth.role !== "string") throw TypeError("auth.role must be a string");
  else if (!auth.username || typeof auth.username !== "string") throw TypeError("auth.username must be a string");
  else if (!auth.token || typeof auth.token !== "string") throw TypeError("auth.token must be a string");
  return new Promise(async function (resolve, reject) {
    const callId = Token.generate();
    const timeout = setTimeout(function () {
      self.authEmitter.removeAllListeners(callId);
      return resolve({
        "callId": callId,
        "type": "deauthenticate",
        "code": "timed-out"
      });
    }, self.rpc.timeout);
    self.authEmitter.prependOnceListener(callId, async function (data) {
      clearTimeout(timeout);
      return resolve({
        "callId": callId,
        "type": data.type,
        "code": data.code
      });
    });
    try {
      await Utility.send(self.client, {
        "callId": callId,
        "type": "deauthenticate",
        "auth": {
          "role": auth.role,
          "username": auth.username,
          "token": auth.token
        }
      });
    } catch (error) {
      clearTimeout(timeout);
      self.rpcEmitter.removeAllListeners(callId);
      return reject(error);
    }
  });
};

WebSocksClient.prototype.setDefaultAuth = function (auth) {
  this.auth = auth;
};

WebSocksClient.prototype.listRPC = async function (auth) {
  const self = this;
  if (auth === true && self.auth && typeof self.auth === "object") auth = self.auth;
  else if (auth === true && (!self.auth || typeof self.auth !== "object")) {
    throw new Error("client default auth must be set!");
  }
  return new Promise(async function (resolve, reject) {
    const callId = Token.generate();
    const timeout = setTimeout(function () {
      self.rpcEmitter.removeAllListeners(callId);
      return resolve({
        "callId": callId,
        "type": "list-rpc",
        "code": "timed-out"
      });
    }, self.rpc.timeout);
    self.rpcEmitter.prependOnceListener(callId, async function (data) {
      clearTimeout(timeout);
      return resolve(data);
    });
    try {
      await Utility.send(self.client, {
        "callId": callId,
        "type": "list-rpc",
        "auth": auth ? {
          "role": auth.role,
          "username": auth.username,
          "password": auth.password,
          "token": auth.token
        } : null
      });
    } catch (error) {
      clearTimeout(timeout);
      self.rpcEmitter.removeAllListeners(callId);
      return reject(error);
    }
  });
};

WebSocksClient.prototype.callRPC = async function (method, data, auth) {
  const self = this;
  if (auth === true && self.auth && typeof self.auth === "object") auth = self.auth;
  if (auth === true && (!self.auth || typeof self.auth !== "object")) {
    throw new Error("client default auth must be set!");
  }
  return new Promise(async function (resolve, reject) {
    const callId = Token.generate();
    const timeout = setTimeout(function () {
      self.rpcEmitter.removeAllListeners(callId);
      return resolve({
        "callId": callId,
        "type": "call-rpc",
        "code": "timed-out"
      });
    }, self.rpc.timeout);
    self.rpcEmitter.prependOnceListener(callId, async function (data) {
      clearTimeout(timeout);
      return resolve(data);
    });
    try {
      await Utility.send(self.client, {
        "callId": callId,
        "type": "call-rpc",
        "auth": auth ? {
          "role": auth.role,
          "username": auth.username,
          "password": auth.password,
          "token": auth.token
        } : null,
        "method": method,
        "data": data
      });
    } catch (error) {
      clearTimeout(timeout);
      self.rpcEmitter.removeAllListeners(callId);
      return reject(error);
    }
  });
};

WebSocksClient.prototype.close = async function () {
  this.client.close(0, "client-closed");
  return Promise.resolve();
};

WebSocksClient.prototype.terminate = async function () {
  this.client.terminate(); // No callbacks ('ws' library issue)
  return Promise.resolve();
};

// Exports
module.exports = WebSocksClient;