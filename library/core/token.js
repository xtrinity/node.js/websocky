"use strict";

// Dependencies

// References
let counter = 0;

function generate() {
  if (counter >= Number.MAX_SAFE_INTEGER) counter = 0;
  else counter++;
  const timestampHex = new Date().getTime().toString(16);
  const randomHex = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER).toString(16);
  return (timestampHex + randomHex + counter.toString(16));
}

// Exports
module.exports = {};
module.exports.generate = generate;