"use strict";

// Dependency
const Client = require("./core/client");

// Reference
const clients = {};

// Function
async function createClient(config, name) {
  if (typeof name !== "string") name = "default";
  if (!config || typeof config !== "object") throw new Error("Config does not exist!");
  const client = new Client(config);
  if (clients[name] && typeof client === "object") throw new Error("Client already exists!");
  clients[name] = client;
  return client;
}

async function getClient(name) {
  if (typeof name !== "string") name = "default";
  return clients[name];
}

async function closeClient(name) {
  if (typeof name !== "string") name = "default";
  const client = clients[name];
  if (!client || typeof client !== "object") throw new Error("Client does not exist or was already closed!");
  await client.close();
  delete clients[name];
  return true;
}

// Export
module.exports = {};
module.exports.createClient = createClient;
module.exports.getClient = getClient;
module.exports.closeClient = closeClient;