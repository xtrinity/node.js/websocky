"use strict";

// Constant
const TIMEOUT_FACTOR = 2;
const DEFAULT_REAUTH_TIMEOUT = 1000 * 60 * 5; // Every 5 minutes

// Reference
const authDb = {};

// Function
async function createAuth(client, auth, option) {
  if (authDb.hasOwnProperty(auth.username) && authDb[auth.username].reauth === false) {
    throw new Error(`${auth.username} has already been authenticated!`);
  }
  const authResponse = await client.authenticate(auth);
  if (authResponse.code !== "success") throw new Error("Client failed to authenticate auth!");
  authDb[auth.username] = authResponse.auth;
  // Reauthentication
  option = option || {};
  authDb[auth.username].reauth = true;
  authDb[auth.username].reauthTimeout = option.reauthTimeout || DEFAULT_REAUTH_TIMEOUT;
  if (authResponse.expireDate instanceof Date && option.timeoutFactor) {
    authDb[auth.username].timeoutFactor = option.timeoutFactor || TIMEOUT_FACTOR;
    authDb[auth.username].reauthTimeout = Math.abs(new Date(authResponse.expireDate) - new Date()) / authDb[auth.username].timeoutFactor;
  }
  authDb[auth.username].timeout = setTimeout(async function () {
    try {
      await createAuth(client, authResponse.auth, option);
    } catch (error) {
      if (typeof option.errorHandler === "function") await option.errorHandler(error);
      else throw error;
    }
  }, authDb[auth.username].reauthTimeout);
}

async function destroyAuth(client, auth) {
  if (!authDb.hasOwnProperty(auth.username)) {
    throw new Error(`${auth.username} has already been deauthenticated or does not exist!`);
  }
  const anAuth = authDb[auth.username];
  const authResponse = await client.deauthenticate(anAuth);
  if (authResponse.code !== "success") throw new Error("Client failed to deauthenticate auth!");
  clearTimeout(authDb[auth.username].timeout);
  delete authDb[auth.username];
}

function getAuth(authRole) {
  for (let element in authDb) {
    if (!authDb.hasOwnProperty(element)) continue;
    if (authDb[element].role === authRole) return authDb[element];
  }
  throw new Error(`Auth with ${authRole} role was not found!`);
}

function getAuthByUsername(authUsername) {
  if (!authDb.hasOwnProperty(authUsername)) throw new Error(`${authUsername} has not been authenticated!`);
  else return authDb[authUsername];
}

// Export
module.exports = {};
module.exports.createAuth = createAuth;
module.exports.destroyAuth = destroyAuth;
module.exports.getAuth = getAuth;
module.exports.getAuthByUsername = getAuthByUsername;