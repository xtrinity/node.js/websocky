# Websocky - TODO

* Server PubSub register subscriptions
* Client PubSub publish to subscription
* Custom websocket header cookie to deal with reverse proxy load balancing for subscription persistence across clients
  - On HTTP error Nginx load-balancing should automatically try the next server
* Add try-catch blocks for error-handling
* Deregister RPC calls
* Ability to throttle bad RPC auth checks
* https://github.com/websockets/ws#how-to-detect-and-close-broken-connections
* Similar to 'client.js' there is a WebSocket "close" problem that could theoretically be exploited for DoS!
* Include support for `remoteAddress` logging
* Support for timing out for `connection` events is missing

## Optimization

* Optimizing Token generation for faster response times
* Optimizing JSON.stringify() for faster response times