"use strict";

// Dependency
const WebSocksServer = require("../index").Server;
const moment = require("moment");

// Process
(async function () {
  const server = new WebSocksServer({
    "hostname": "localhost",
    "port": 8080,
    "authFunction": authFunction,
    "deauthFunction": deauthFunction,
  });
  server.registerRPC("public", function (data) {
    return {
      "code": "success",
      "data": {
        "message": "Successful use of public method!",
        "received": data
      }
    };
  });
  server.registerRPC("private", function (data) {
    return {
      "code": "success",
      "data": {
        "message": "Successful use of private method!",
        "received": data
      }
    };
  }, "test-role");
  await server.initialize();
})();

// Function
function authFunction(auth) {
  if (typeof auth.password === "string") {
    if (auth.role === "test-role" && auth.username === "test-username") {
      return {
        "role": auth.role,
        "username": auth.username,
        "token": "test-token",
        "expires": moment().utc().add(1, "day").toDate()
      };
    }
  } else if (typeof auth.token === "string") {
    if (auth.role === "test-role" && auth.username === "test-username") {
      return {
        "role": auth.role,
        "username": auth.username,
        "token": "test-token",
        "expires": moment().utc().add(1, "day").toDate()
      };
    }
  }
  return false;
}

function deauthFunction(auth) {
  return auth.role === "test-role" && auth.username === "test-username" && auth.token === "test-token";
}