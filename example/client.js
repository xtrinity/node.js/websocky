"use strict";

// Dependencies
const ClientManager = require("../index").ClientManager;
const AuthManager = require("../index").AuthManager;

// Process
(async function () {
  const client = await ClientManager.createClient({
    "hostname": "localhost",
    "port": 8080,
    "rpcTimeout": 1000 * 30
  });
  await client.connect();
  await AuthManager.createAuth(client, {
    "role": "test-role",
    "username": "test-username",
    "password": "test-password",
    "reauthTimeout": 1000 * 60 * 60 * 6
  });
  const publicMethodResponse = await client.callRPC("public", {
    "some-public-data": "some-public-data"
  });
  console.dir(publicMethodResponse);
  const privateMethodResponse = await client.callRPC("private", {
    "some-private-data": "some-private-data"
  }, AuthManager.getAuth("test"));
  console.dir(privateMethodResponse);
  const privateMethodResponse2 = await client.callRPC("private", {
    "some-private-data": "some-private-data"
  }, AuthManager.getAuth("test-role"));
  console.dir(privateMethodResponse2);
})();