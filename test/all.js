"use strict";

// Dependency
const expect = require("chai").expect;
const WebSocksServer = require("../index").Server;
const ClientManager = require("../index").ClientManager;
const AuthManager = require("../index").AuthManager;
const moment = require("moment");
const assert = require("assert");

// Reference
let server = null;
let client = null;

// Test
describe("WebSocketServer", async function () {
  it("instantiate", async function () {
    server = new WebSocksServer({
      "hostname": "localhost",
      "port": 8080,
      "authFunction": function (auth) {
        if (typeof auth.password === "string") {
          if (auth.role === "test-role" && auth.username === "test-username") {
            return {
              "role": auth.role,
              "username": auth.username,
              "token": "test-token",
              "expires": moment().utc().add(1, "day").toDate()
            };
          }
        } else if (typeof auth.token === "string") {
          if (auth.role === "test-role" && auth.username === "test-username") {
            return {
              "role": auth.role,
              "username": auth.username,
              "token": "test-token",
              "expires": moment().utc().add(1, "day").toDate()
            };
          }
        }
        return false;
      },
      "deauthFunction": function (auth) {
        return auth.role === "test-role" && auth.username === "test-username" && auth.token === "test-token";
      }
    });
  });
  it("register public rpc", async function () {
    server.registerRPC("public", function (data) {
      return {
        "code": "success",
        "data": {
          "received": data
        }
      };
    });
  });
  it("register private rpc", async function () {
    server.registerRPC("private", function (data) {
      return {
        "code": "success",
        "data": {
          "received": data
        }
      };
    }, "test-role");
  });
  it("initialize", async function () {
    await server.initialize();
  });
});

describe("WebSocketClient Standard", async function () {
  let client = null;
  it("instantiate", async function () {
    client = await ClientManager.createClient({
      "hostname": "localhost",
      "port": 8080,
      "rpcTimeout": 1000 * 30
    });
  });
  it("connect", async function () {
    return client.connect();
  });
  let auth = null;
  let badAuth = {
    "role": "bad-auth",
    "username": "bad-auth",
    "password": "bad-auth",
    "token": "bad-auth",
    "reauthPeriod": 1000 * 60 * 60 * 6
  };
  it("create auth and authenticate", async function () {
    auth = (await client.authenticate({
      "role": "test-role",
      "username": "test-username",
      "password": "test-password",
      "reauthPeriod": 1000 * 60 * 60 * 6
    })).auth;
  });
  it("set client default auth", async function () {
    client.setAuth(auth);
  });
  it("call publicMethod with correct method", async function () {
    const response = await client.callRPC("public", {
      "correct": "correct"
    });
    assert(response && typeof response === "object");
    assert(response.callId && typeof response.callId === "string");
    assert(response.code === "success");
    assert(typeof response.data === "object");
    assert(response.data.received.correct === "correct");
  });
  it("call unregistered publicMethod", async function () {
    const response = await client.callRPC("unknown", {
      "unknown": "unknown"
    });
    assert(response && typeof response === "object");
    assert(response.callId && typeof response.callId === "string");
    assert(response.type === "call-rpc");
    assert(response.code === "invalid-method");
  });
  it("call privateMethod with bad auth", async function () {
    const response = await client.callRPC("private", {
      "correct": "correct"
    }, badAuth);
    assert(response && typeof response === "object");
    assert(response.callId && typeof response.callId === "string");
    assert(response.type === "call-rpc");
    assert(response.code === "invalid-auth");
  });
  it("call privateMethod with bad auth and unknown method", async function () {
    const response = await client.callRPC("unknown", {
      "correct": "correct"
    }, badAuth);
    assert(response && typeof response === "object");
    assert(response.callId && typeof response.callId === "string");
    assert(response.type === "call-rpc");
    assert(response.code === "invalid-auth");
  });
  it("call privateMethod with unknown method", async function () {
    const response = await client.callRPC("unknown", {
      "correct": "correct"
    }, auth);
    assert(response && typeof response === "object");
    assert(response.callId && typeof response.callId === "string");
    assert(response.type === "call-rpc");
    assert(response.code === "invalid-method");
  });
  it("call privateMethod with default auth", async function () {
    const response = await client.callRPC("private", {
      "correct": "correct"
    }, true);
    assert(response && typeof response === "object");
    assert(response.callId && typeof response.callId === "string");
    assert(response.type === "call-rpc");
    assert(response.code === "success");
    assert(typeof response.data === "object");
    assert(response.data.received.correct === "correct");
  });
  it("call privateMethod with correct method", async function () {
    const response = await client.callRPC("private", {
      "correct": "correct"
    }, auth);
    assert(response && typeof response === "object");
    assert(response.callId && typeof response.callId === "string");
    assert(response.type === "call-rpc");
    assert(response.code === "success");
    assert(typeof response.data === "object");
    assert(response.data.received.correct === "correct");
  });
  //standardTests(client);
  // authManagerTests();
});

// Function

function authManagerTests() {
  describe("AuthManager Tests", async function () {
    await server.terminate();

  });
  // TODO:
  // Test AuthManager multiple storage
  // Test AuthManager refresh
  // Test Server auth.db multiple storage and clearing
}