# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2020-04-15
### Added
- File size logging

## [1.0.0] - 2020-02-07
### Added
- Logging
- Ignore logging for some methods

## [0.0.1] - 2019-11-04
### Added
- Server register RPC
- Client call RPC
